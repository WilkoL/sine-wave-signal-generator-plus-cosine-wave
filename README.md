# Sine wave signal generator - plus cosine wave

STM32F407VGT at 168 MHz and a ST7735 TFT display (1.8 inch)

It use the DDS method of generating both a sine wave and cosine wave.
Both waves have the same frequency, amplitude and offset but are 
always 90 degrees out of phase.

Frequency from 0.01 Hz to 999,999.99 Hz (1 MHz)
Amplitude variable in 3dB steps to 5V tt.
Offset from -5V to 5V