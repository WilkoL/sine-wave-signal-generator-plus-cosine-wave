#ifndef __ST7735_H__
#define __ST7735_H__

#include "main.h"
#include <string.h>
#include "font5x7.h"
#include "font7x11.h"
#include "color565.h"

// Screen resolution in normal orientation
#define scr_w         128
#define scr_h         160

//here we use SPI3

#define ST7735_PORT		GPIOA
#define ST7735_RST		LL_GPIO_PIN_11
#define ST7735_A0		LL_GPIO_PIN_12
#define ST7735_CS		LL_GPIO_PIN_15


// CS pin macros
#define CS_L() LL_GPIO_ResetOutputPin(ST7735_PORT, ST7735_CS)
#define CS_H() LL_GPIO_SetOutputPin(ST7735_PORT, ST7735_CS)

// A0 pin macros
#define A0_L() LL_GPIO_ResetOutputPin(ST7735_PORT, ST7735_A0)
#define A0_H() LL_GPIO_SetOutputPin(ST7735_PORT, ST7735_A0)

// RESET pin macros
#define RST_L() LL_GPIO_ResetOutputPin(ST7735_PORT, ST7735_RST)
#define RST_H() LL_GPIO_SetOutputPin(ST7735_PORT, ST7735_RST)

// Colors for spaces between symbols for debug view
//#define V_SEP COLOR565_YELLOW
//#define H_SEP COLOR565_SIENNA
//#define V_SEP COLOR565_WHITE
//#define H_SEP COLOR565_WHITE

typedef enum
{
	scr_normal = 0,
	scr_CW     = 1,
	scr_CCW    = 2,
	scr_180    = 3
} ScrOrientation_TypeDef;

extern uint16_t scr_width;
extern uint16_t scr_height;

uint16_t RGB565(uint8_t R,uint8_t G,uint8_t B);
void ST7735_write(uint8_t data);
void ST7735_Init(void);
void ST7735_AddrSet(uint16_t XS, uint16_t YS, uint16_t XE, uint16_t YE);
void ST7735_Orientation(uint8_t orientation);
void ST7735_Clear(uint16_t color);
void ST7735_Pixel(uint16_t X, uint16_t Y, uint16_t color);
void ST7735_HLine(uint16_t X1, uint16_t X2, uint16_t Y, uint16_t color);
void ST7735_VLine(uint16_t X, uint16_t Y1, uint16_t Y2, uint16_t color);
void ST7735_Line(int16_t X1, int16_t Y1, int16_t X2, int16_t Y2, uint16_t color);
void ST7735_Rect(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t color);
void ST7735_FillRect(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t color);
void ST7735_PutChar5x7(uint8_t scale, uint16_t X, uint16_t Y, uint8_t chr, uint16_t color, uint16_t bgcolor);
void ST7735_PutStr5x7(uint8_t scale, uint8_t X, uint8_t Y, char *str, uint16_t color, uint16_t bgcolor);
void ST7735_PutChar7x11(uint16_t X, uint16_t Y, uint8_t chr, uint16_t color, uint16_t bgcolor);
void ST7735_PutStr7x11(uint8_t X, uint8_t Y, char *str, uint16_t color, uint16_t bgcolor);


#endif
