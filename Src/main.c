/*
 * signal generator sine and cosine wave
 * STM32F407VGT @ 168 MHz
 *
 * DDS method of generating both a sine wave and cosine wave
 * both channels always have the same frequency, amplitude and offset
 * but are 90 degrees out of phase
 *
 * ST7735 display
 * Rotary encoder with push button
 *
 * defaults:
 * waveform		sinewave and co-sinewave
 * amplitude	4 Volt tt
 * offset 		0 V
 * frequency	1 kHz
 */




//#define STEPS 256
#define STEPS 1024


#include "main.h"
#include "stdio.h"
#include "stdlib.h"

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_DAC_Init(void);
static void MX_SPI3_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM7_Init(void);
static void MX_TIM8_Init(void);

void DMA1_Stream5_IRQHandler(void);

int8_t ROTARY_direction(void);
int32_t CALC_frequency(int32_t freq_in, int8_t direction, uint8_t dec);
void SHOW_frequency(uint32_t freq);
void SHOW_cursor(uint8_t position);



uint32_t waveform[STEPS];										//will be put in CCMRAM
uint32_t dac_buffer_a[STEPS] __attribute__((section("ram")));	//in RAM because of DMA
uint32_t dac_buffer_b[STEPS] __attribute__((section("ram")));	//in RAM because of DMA
uint64_t stepsize;

int main(void)
{
	char buff[20];

	uint8_t button = 1;
	uint8_t previous_button = 1;

	uint16_t start_time = 0;
	uint16_t elapsed_time = 0;

	uint8_t frequency_decimal = 5;
	uint8_t cursor_position = 5;

	int8_t amplitude = 15;									//max amplitude
	int8_t previous_amplitude = 15;
	int8_t offset = 25;										//0 Volt
	int8_t previous_offset = 25;
	int8_t up_down = 0;

	int32_t frequency = 100000;
	int32_t previous_frequency = 0;
	//uint32_t const reference = 699994750;					//~7 MHz

	  uint32_t const reference = 793675264;		//168MHz / 21
	//uint32_t const reference = 884210526;		//168MHz / 19
	//uint32_t const reference = 933333333;		//168MHz / 18

	stepsize = (uint64_t) (frequency * 4294967296) / reference;

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

	SystemClock_Config();
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_DAC_Init();
	MX_TIM3_Init();
	MX_TIM4_Init();
	MX_TIM7_Init();
	MX_TIM8_Init();
	MX_SPI3_Init();

	LL_SPI_Enable(SPI3);

	LL_DMA_EnableDoubleBufferMode(DMA2, LL_DMA_STREAM_1);
	LL_DMA_SetPeriphAddress(DMA2, LL_DMA_STREAM_1, (uint32_t) &DAC1->DHR12RD);
	LL_DMA_SetMemoryAddress(DMA2, LL_DMA_STREAM_1, (uint32_t) &dac_buffer_a[0]);		//memory address 0
	LL_DMA_SetMemory1Address(DMA2, LL_DMA_STREAM_1, (uint32_t) &dac_buffer_b[0]);		//memory address 1
	LL_DMA_SetDataLength(DMA2, LL_DMA_STREAM_1, STEPS);
	LL_DMA_EnableIT_TC(DMA2, LL_DMA_STREAM_1);
	LL_DMA_EnableStream(DMA2, LL_DMA_STREAM_1);				//setup DMA things first


	//LL_DAC_EnableTrigger(DAC1, LL_DAC_CHANNEL_1);
	//LL_DAC_EnableTrigger(DAC1, LL_DAC_CHANNEL_2);

	//LL_DAC_EnableDMAReq(DAC1, LL_DAC_CHANNEL_1);
	//LL_DAC_EnableDMAReq(DAC1, LL_DAC_CHANNEL_2);

	LL_DAC_Enable(DAC1, LL_DAC_CHANNEL_1);					//do this last
	LL_DAC_Enable(DAC1, LL_DAC_CHANNEL_2);

	LL_TIM_EnableCounter(TIM3);								//PWM for offset voltage

	LL_TIM_EnableCounter(TIM4);								//rotary encoder
	LL_TIM_SetCounter(TIM4, 100);

	LL_TIM_EnableCounter(TIM7);								//timeout

	LL_TIM_EnableCounter(TIM8);								//triggering DAC
	LL_TIM_EnableUpdateEvent(TIM8);
	LL_TIM_EnableAllOutputs(TIM8);
	LL_TIM_EnableDMAReq_UPDATE(TIM8);


	//copy sinus_cosinus[] from FLASH to CCMRAM
	for (uint16_t i=0; i<STEPS; i++) waveform[i] = sinus_cosinus_1024[i];

	ST7735_Init();
	ST7735_AddrSet(0,0,159,127);
	ST7735_Clear(COLOR565_DARK_BLUE);
	ST7735_Orientation(scr_CCW);

	ST7735_PutStr5x7(2, 0, 10,  " CO-SINEWAVE", COLOR565_WHITE, COLOR565_DARK_BLUE);
	SHOW_cursor(cursor_position);

	sprintf(buff, "amplitude %d", amplitude+1);
	ST7735_PutStr5x7(2, 1, 90, buff, COLOR565_WHITE, COLOR565_DARK_BLUE);

	sprintf(buff, "offset    %d", offset-25);
	ST7735_PutStr5x7(2, 1, 110, buff, COLOR565_WHITE, COLOR565_DARK_BLUE);


	while (1)
	{
		button = LL_GPIO_IsInputPinSet(GPIOD, LL_GPIO_PIN_11);
		if ((button == 0) && (previous_button == 1))
		{
			LL_mDelay(10);									//anti_bounce
			start_time = LL_TIM_GetCounter(TIM7);			//time at button press
			while (LL_GPIO_IsInputPinSet(GPIOD, LL_GPIO_PIN_11) == 0);//wait for button released
			elapsed_time = LL_TIM_GetCounter(TIM7) - start_time;

			if (elapsed_time <= 640)						//less than 0.5 s
			{												//change frequency-decimal
				if (frequency_decimal > 0) frequency_decimal--;
				else frequency_decimal = 7;
				SHOW_cursor(frequency_decimal);
			}

			else if (elapsed_time <= 2560)					//0.5 < time < 2 s
			{												//change amplitude
				start_time = LL_TIM_GetCounter(TIM7);
				elapsed_time = 0;
				previous_amplitude = 100;					//always different from real amplitude

				while (elapsed_time < 5120)					//4 seconds
				{
					if (amplitude != previous_amplitude)
					{
						ST7735_PutStr5x7(2, 1, 90, "amplitude    ", COLOR565_DARK_BLUE, COLOR565_WHITE);
						sprintf(buff, "amplitude %d", amplitude+1);
						ST7735_PutStr5x7(2, 1, 90, buff, COLOR565_DARK_BLUE, COLOR565_WHITE);
						previous_amplitude = amplitude;
					}

					up_down = ROTARY_direction();
					if (up_down != 0)
					{
						LL_TIM_SetCounter(TIM4, 100);
						start_time = LL_TIM_GetCounter(TIM7);
						amplitude += up_down;
						if (amplitude < 0) amplitude = 0;
						if (amplitude > 15) amplitude = 15;

						if (amplitude & 0x01) LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_11);
						else LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_11);

						if (amplitude & 0x02) LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_12);
						else LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_12);

						if (amplitude & 0x04) LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_13);
						else LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_13);

						if (amplitude & 0x08) LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_14);
						else LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_14);
					}

					elapsed_time = LL_TIM_GetCounter(TIM7) - start_time;
				}
				sprintf(buff, "amplitude %d", amplitude+1);
				ST7735_PutStr5x7(2, 1, 90, "             ", COLOR565_WHITE, COLOR565_DARK_BLUE);
				ST7735_PutStr5x7(2, 1, 90, buff, COLOR565_WHITE, COLOR565_DARK_BLUE);
			}

			else 											//longer than 2 s
			{												//change offset
				start_time = LL_TIM_GetCounter(TIM7);
				elapsed_time = 0;
				previous_offset = 100;						//always different from the real offset
				//ST7735_PutStr5x7(2, 1, 110,"             ", COLOR565_DARK_BLUE, COLOR565_WHITE);

				while (elapsed_time < 5120)					//4 seconds
				{
					if (previous_offset != offset)
					{
						ST7735_PutStr5x7(2, 1, 110, "offset       ", COLOR565_DARK_BLUE, COLOR565_WHITE);
						sprintf(buff, "offset    %d", offset-25);
						ST7735_PutStr5x7(2, 1, 110, buff, COLOR565_DARK_BLUE, COLOR565_WHITE);
						previous_offset = offset;
					}

					up_down = ROTARY_direction();
					if (up_down != 0)
					{
						LL_TIM_SetCounter(TIM4, 100);
						start_time = LL_TIM_GetCounter(TIM7);
						offset += up_down;
						if (offset < 1) offset = 1;
						if (offset > 49) offset = 49;
						LL_TIM_OC_SetCompareCH4(TIM3, ((offset * 20) - 10));
					}
					elapsed_time = LL_TIM_GetCounter(TIM7) - start_time;
				}

				sprintf(buff, "offset    %d", offset-25);
				ST7735_PutStr5x7(2, 1, 110,"             ", COLOR565_WHITE, COLOR565_DARK_BLUE);
				ST7735_PutStr5x7(2, 1, 110,buff, COLOR565_WHITE, COLOR565_DARK_BLUE);
			}
		}
		previous_button = button;




		up_down = ROTARY_direction();							//read rotary movement
		if (up_down != 0)
		{
			LL_TIM_SetCounter(TIM4, 100);
			frequency = CALC_frequency(frequency, up_down, frequency_decimal);
		}
		if (frequency != previous_frequency)
		{
			stepsize = (uint64_t) (frequency * 4294967296) / reference;
			SHOW_frequency(frequency);
			previous_frequency = frequency;
		}
	}
}


int8_t ROTARY_direction(void)
{
	int8_t rotary_direction = 0;	//0 = no change, 1 = up, -1 = down
	uint16_t rotary_value = 0;

	rotary_value = LL_TIM_GetCounter(TIM4);
	if (rotary_value < 99) rotary_direction = -1;
	if (rotary_value > 101) rotary_direction = 1;

	return rotary_direction;
}


int32_t CALC_frequency(int32_t freq_in, int8_t direction, uint8_t dec)
{
	int32_t freq_out = 0;

	switch (dec)
	{
	case 0:
		freq_out = freq_in + (direction * 1);
		break;
	case 1:
		freq_out = freq_in + (direction * 10);
		break;
	case 2:
		freq_out = freq_in + (direction * 100);
		break;
	case 3:
		freq_out = freq_in + (direction * 1000);
		break;
	case 4:
		freq_out = freq_in + (direction * 10000);
		break;
	case 5:
		freq_out = freq_in + (direction * 100000);
		break;
	case 6:
		freq_out = freq_in + (direction * 1000000);
		break;
	case 7:
		freq_out = freq_in + (direction * 10000000);
		break;
	default:
		break;
	}
	if (freq_out < 0) freq_out = 0;
	if (freq_out > 99999999) freq_out = 99999999;
	return freq_out;
}


void SHOW_frequency(uint32_t freq)
{
	char buffer[20];
	uint8_t cijfer[8];

	if (freq <= 99999999)
	{
		for (uint8_t i = 0; i < 8; i++)
		{
			cijfer[i] = (uint8_t) (freq % 10) + 48;
			freq /= 10;
		}
		sprintf(buffer, "%c%c%c %c%c%c.%c%c Hz", cijfer[7], cijfer[6], cijfer[5], cijfer[4], cijfer[3], cijfer[2], cijfer[1], cijfer[0]);
		ST7735_PutStr5x7(2, 1, 50, buffer, COLOR565_WHITE, COLOR565_DARK_BLUE);
	}
	else ST7735_PutStr5x7(2, 1, 50, "   Error", COLOR565_WHITE, COLOR565_DARK_BLUE);
}


void SHOW_cursor(uint8_t position)
{
	uint8_t cursor_position = 8;

	switch (position)
	{
	case 0:
		cursor_position = 1+9*12;
		break;
	case 1:
		cursor_position = 1+8*12;
		break;
	case 2:
		cursor_position = 1+6*12;
		break;
	case 3:
		cursor_position = 1+5*12;
		break;
	case 4:
		cursor_position = 1+4*12;
		break;
	case 5:
		cursor_position = 1+2*12;
		break;
	case 6:
		cursor_position = 1+1*12;
		break;
	case 7:
		cursor_position = 1+0*12;
		break;
	default:
		break;
	}
	ST7735_PutStr5x7(2, 1, 70, "              ", COLOR565_WHITE, COLOR565_DARK_BLUE);
	ST7735_PutStr5x7(2, cursor_position, 70, "^", COLOR565_WHITE, COLOR565_DARK_BLUE);
}




void DMA2_Stream1_IRQHandler(void)
{
	static uint32_t phase = 0;
	uint16_t x;

	LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_0);

	if (LL_DMA_IsActiveFlag_TC1(DMA2))
	{
		LL_DMA_ClearFlag_TC1(DMA2);
		if (LL_DMA_GetCurrentTargetMem(DMA2, LL_DMA_STREAM_1))
		{
			for (x = 0; x < STEPS; x++)
			{
				dac_buffer_a[x] = waveform[phase >> 22];
				phase += stepsize;
			}
		}
		else
		{
			for (x = 0; x < STEPS; x++)
			{
				dac_buffer_b[x] = waveform[phase >> 22];
				phase += stepsize;
			}
		}
	}

	LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_0);
}


void SystemClock_Config(void)								//onboard ceramic resonator 8 MHz
{
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_5);
	if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_5) Error_Handler();
	LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE1);
	LL_FLASH_EnableInstCache();
	LL_FLASH_EnablePrefetch();
	LL_RCC_HSE_Enable();
	while(LL_RCC_HSE_IsReady() != 1);
	LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_4, 168, LL_RCC_PLLP_DIV_2);
	LL_RCC_PLL_Enable();
	while(LL_RCC_PLL_IsReady() != 1);
	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_4);
	LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_2);
	LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL);
	LL_Init1msTick(168000000);
	LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
	LL_SetSystemCoreClock(168000000);
}


static void MX_SPI3_Init(void)
{
	LL_SPI_InitTypeDef SPI_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI3);		//42 MHz
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_10|LL_GPIO_PIN_12;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
	LL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	SPI_InitStruct.TransferDirection = LL_SPI_FULL_DUPLEX;
	SPI_InitStruct.Mode = LL_SPI_MODE_MASTER;
	SPI_InitStruct.DataWidth = LL_SPI_DATAWIDTH_8BIT;
	SPI_InitStruct.ClockPolarity = LL_SPI_POLARITY_LOW;
	SPI_InitStruct.ClockPhase = LL_SPI_PHASE_1EDGE;
	SPI_InitStruct.NSS = LL_SPI_NSS_SOFT;
	SPI_InitStruct.BaudRate = LL_SPI_BAUDRATEPRESCALER_DIV8;	//5.25 MHz
	SPI_InitStruct.BitOrder = LL_SPI_MSB_FIRST;
	SPI_InitStruct.CRCCalculation = LL_SPI_CRCCALCULATION_DISABLE;
	SPI_InitStruct.CRCPoly = 10;
	LL_SPI_Init(SPI3, &SPI_InitStruct);
	LL_SPI_SetStandard(SPI3, LL_SPI_PROTOCOL_MOTOROLA);
}


static void MX_DAC_Init(void)
{
	LL_DAC_InitTypeDef DAC_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_DAC1);		//42 MHz
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_4|LL_GPIO_PIN_5;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	DAC_InitStruct.TriggerSource = LL_DAC_TRIG_SOFTWARE;
	DAC_InitStruct.WaveAutoGeneration = LL_DAC_WAVE_AUTO_GENERATION_NONE;
	DAC_InitStruct.OutputBuffer = LL_DAC_OUTPUT_BUFFER_DISABLE;
	LL_DAC_Init(DAC, LL_DAC_CHANNEL_1, &DAC_InitStruct);

	DAC_InitStruct.TriggerSource = LL_DAC_TRIG_SOFTWARE;
	DAC_InitStruct.WaveAutoGeneration = LL_DAC_WAVE_AUTO_GENERATION_NONE;
	DAC_InitStruct.OutputBuffer = LL_DAC_OUTPUT_BUFFER_DISABLE;
	LL_DAC_Init(DAC, LL_DAC_CHANNEL_2, &DAC_InitStruct);
}


static void MX_TIM3_Init(void)								//PWM for offset voltage
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);		//84 MHz
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);

	TIM_InitStruct.Prescaler = 0;							//84 MHz
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 999;						//84 kHz
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM3, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM3);
	LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_OC_EnablePreload(TIM3, LL_TIM_CHANNEL_CH4);

	TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM1;
	TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_ENABLE;
	TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.CompareValue = 490;					//duty cycle 49%
	TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH;
	LL_TIM_OC_Init(TIM3, LL_TIM_CHANNEL_CH4, &TIM_OC_InitStruct);

	LL_TIM_OC_DisableFast(TIM3, LL_TIM_CHANNEL_CH4);
	LL_TIM_SetTriggerOutput(TIM3, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM3);


	GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_2;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}


static void MX_TIM4_Init(void)								//rotary encoder
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM4);

	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOD);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_12|LL_GPIO_PIN_13;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_2;
	LL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	LL_TIM_SetEncoderMode(TIM4, LL_TIM_ENCODERMODE_X2_TI1);
	LL_TIM_IC_SetActiveInput(TIM4, LL_TIM_CHANNEL_CH1, LL_TIM_ACTIVEINPUT_DIRECTTI);
	LL_TIM_IC_SetPrescaler(TIM4, LL_TIM_CHANNEL_CH1, LL_TIM_ICPSC_DIV1);
	LL_TIM_IC_SetFilter(TIM4, LL_TIM_CHANNEL_CH1, LL_TIM_IC_FILTER_FDIV32_N8);
	LL_TIM_IC_SetPolarity(TIM4, LL_TIM_CHANNEL_CH1, LL_TIM_IC_POLARITY_RISING);
	LL_TIM_IC_SetActiveInput(TIM4, LL_TIM_CHANNEL_CH2, LL_TIM_ACTIVEINPUT_DIRECTTI);
	LL_TIM_IC_SetPrescaler(TIM4, LL_TIM_CHANNEL_CH2, LL_TIM_ICPSC_DIV1);
	LL_TIM_IC_SetFilter(TIM4, LL_TIM_CHANNEL_CH2, LL_TIM_IC_FILTER_FDIV32_N8);
	LL_TIM_IC_SetPolarity(TIM4, LL_TIM_CHANNEL_CH2, LL_TIM_IC_POLARITY_RISING);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 0xFFFF;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM4, &TIM_InitStruct);
	LL_TIM_DisableARRPreload(TIM4);
	LL_TIM_SetTriggerOutput(TIM4, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM4);
}


static void MX_TIM7_Init(void)								//button timing
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM7);		//84 MHz

	TIM_InitStruct.Prescaler = 0xFFFF;						//approx 1.28 kHz
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 0xFFFF;
	LL_TIM_Init(TIM7, &TIM_InitStruct);
	LL_TIM_DisableARRPreload(TIM7);
	LL_TIM_SetTriggerOutput(TIM7, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM7);
}


static void MX_TIM8_Init(void)								//triggering DAC
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};
	LL_TIM_BDTR_InitTypeDef TIM_BDTRInitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM8);		//168 MHz
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);

	// TIM8 DMA Init
	// TIM8_UP Init
	LL_DMA_SetChannelSelection(DMA2, LL_DMA_STREAM_1, LL_DMA_CHANNEL_7);
	LL_DMA_SetDataTransferDirection(DMA2, LL_DMA_STREAM_1, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
	LL_DMA_SetStreamPriorityLevel(DMA2, LL_DMA_STREAM_1, LL_DMA_PRIORITY_VERYHIGH);
	LL_DMA_SetMode(DMA2, LL_DMA_STREAM_1, LL_DMA_MODE_CIRCULAR);
	LL_DMA_SetPeriphIncMode(DMA2, LL_DMA_STREAM_1, LL_DMA_PERIPH_NOINCREMENT);
	LL_DMA_SetMemoryIncMode(DMA2, LL_DMA_STREAM_1, LL_DMA_MEMORY_INCREMENT);
	LL_DMA_SetPeriphSize(DMA2, LL_DMA_STREAM_1, LL_DMA_PDATAALIGN_WORD);
	LL_DMA_SetMemorySize(DMA2, LL_DMA_STREAM_1, LL_DMA_MDATAALIGN_WORD);
	//LL_DMA_DisableFifoMode(DMA2, LL_DMA_STREAM_1);
	LL_DMA_EnableFifoMode(DMA2, LL_DMA_STREAM_1);

	TIM_InitStruct.Prescaler = 0;							//168 MHz
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;

	//TIM_InitStruct.Autoreload = 15;							//10.5 MHz
	//TIM_InitStruct.Autoreload = 18;							//8.8 MHz
	TIM_InitStruct.Autoreload = 20;							//8 MHz
	//TIM_InitStruct.Autoreload = 23;							//7 MHz
	//TIM_InitStruct.Autoreload = 27;							//6 MHz



	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	TIM_InitStruct.RepetitionCounter = 0;
	LL_TIM_Init(TIM8, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM8);
	LL_TIM_SetClockSource(TIM8, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_OC_EnablePreload(TIM8, LL_TIM_CHANNEL_CH1);

	TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM1;
	TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_ENABLE;
	TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.CompareValue = 9;
	TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH;
	TIM_OC_InitStruct.OCNPolarity = LL_TIM_OCPOLARITY_HIGH;
	TIM_OC_InitStruct.OCIdleState = LL_TIM_OCIDLESTATE_LOW;
	TIM_OC_InitStruct.OCNIdleState = LL_TIM_OCIDLESTATE_LOW;
	LL_TIM_OC_Init(TIM8, LL_TIM_CHANNEL_CH1, &TIM_OC_InitStruct);

	LL_TIM_OC_DisableFast(TIM8, LL_TIM_CHANNEL_CH1);
	LL_TIM_SetTriggerOutput(TIM8, LL_TIM_TRGO_UPDATE);
	LL_TIM_DisableMasterSlaveMode(TIM8);

	TIM_BDTRInitStruct.OSSRState = LL_TIM_OSSR_DISABLE;
	TIM_BDTRInitStruct.OSSIState = LL_TIM_OSSI_DISABLE;
	TIM_BDTRInitStruct.LockLevel = LL_TIM_LOCKLEVEL_OFF;
	TIM_BDTRInitStruct.DeadTime = 0;
	TIM_BDTRInitStruct.BreakState = LL_TIM_BREAK_DISABLE;
	TIM_BDTRInitStruct.BreakPolarity = LL_TIM_BREAK_POLARITY_HIGH;
	TIM_BDTRInitStruct.AutomaticOutput = LL_TIM_AUTOMATICOUTPUT_DISABLE;
	LL_TIM_BDTR_Init(TIM8, &TIM_BDTRInitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_3;
	LL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}


static void MX_DMA_Init(void) 
{
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA2);

	NVIC_SetPriority(DMA2_Stream1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(DMA2_Stream1_IRQn);

	//LL_DMA_SetChannelSelection(DMA1, LL_DMA_STREAM_5, LL_DMA_CHANNEL_7);
	//LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_STREAM_5, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
	//LL_DMA_SetStreamPriorityLevel(DMA1, LL_DMA_STREAM_5, LL_DMA_PRIORITY_VERYHIGH);
	//LL_DMA_SetMode(DMA1, LL_DMA_STREAM_5, LL_DMA_MODE_CIRCULAR);
	//LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_STREAM_5, LL_DMA_PERIPH_NOINCREMENT);
	//LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_STREAM_5, LL_DMA_MEMORY_INCREMENT);
	//LL_DMA_SetPeriphSize(DMA1, LL_DMA_STREAM_5, LL_DMA_PDATAALIGN_WORD);
	//LL_DMA_SetMemorySize(DMA1, LL_DMA_STREAM_5, LL_DMA_MDATAALIGN_WORD);
	//LL_DMA_EnableFifoMode(DMA1, LL_DMA_STREAM_5);
}


static void MX_GPIO_Init(void)
{
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOD);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOE);

	LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_0);
	LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_11);
	LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_12);
	LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_13);
	LL_GPIO_ResetOutputPin(GPIOE, LL_GPIO_PIN_14);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_11|LL_GPIO_PIN_12|LL_GPIO_PIN_13|LL_GPIO_PIN_14;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_11|LL_GPIO_PIN_12|LL_GPIO_PIN_15;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_11;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOD, &GPIO_InitStruct);
}


void Error_Handler(void)
{

}

#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t *file, uint32_t line)
{ 

}
#endif
